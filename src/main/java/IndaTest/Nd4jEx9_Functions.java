/* *****************************************************************************
 * Copyright (c) 2015-2019 Skymind, Inc.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Apache License, Version 2.0 which is available at
 * https://www.apache.org/licenses/LICENSE-2.0.
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
 * License for the specific language governing permissions and limitations
 * under the License.
 *
 * SPDX-License-Identifier: Apache-2.0
 ******************************************************************************/

package IndaTest;

import lombok.extern.slf4j.Slf4j;
import org.deeplearning4j.datasets.iterator.impl.MnistDataSetIterator;
import org.nd4j.linalg.api.ndarray.INDArray;
import org.nd4j.linalg.dataset.DataSet;
import org.nd4j.linalg.dataset.MultiDataSet;
import org.nd4j.linalg.dataset.api.iterator.DataSetIterator;
import org.nd4j.linalg.dataset.api.iterator.KFoldIterator;
import org.nd4j.linalg.dataset.api.preprocessor.NormalizerMinMaxScaler;
import org.nd4j.linalg.factory.Nd4j;
import org.nd4j.nativeblas.Nd4jCpu;

import java.io.IOException;

import static org.nd4j.linalg.ops.transforms.Transforms.*;


/**
 * --- Nd4j Example 9: Functions ---
 *
 * In this example, we'll see how apply some mathematical functions to a matrix
 *
 * Created by cvn on 9/7/14.
 */
@Slf4j
public class Nd4jEx9_Functions {

    public static void main(String[] args) throws IOException {

        DataSetIterator train = new MnistDataSetIterator(30, true, 12345);

        INDArray ax = Nd4j.create(new float[]{1,2,3,4}, 2, 2);
        INDArray ay = Nd4j.create(new float[]{1,2,3,4}, 2, 2);
       // System.out.println(nd.toString());



        INDArray x = Nd4j.create(new float[][]{{1,2},{2,3},{3,4},{4,5}});
        INDArray y = Nd4j.create(new float[][]{{1},{2},{3},{4}});
        DataSet a = new DataSet(x,y);
        DataSet b = new DataSet(ax,ay);
        KFoldIterator Ai = new KFoldIterator(2, a);
        KFoldIterator Bi = new KFoldIterator(2, b);
        NormalizerMinMaxScaler preProcessorIter = new NormalizerMinMaxScaler(0,1);
        preProcessorIter.fit(Ai);
        //Bi.setPreProcessor(preProcessorIter);

        while (Ai.hasNext()){

           /* System.out.println(Ai.next().isPreProcessed());*/

            INDArray firstBatch = Ai.next().getFeatures();
             preProcessorIter.transform(firstBatch);
            log.info("\n{}",firstBatch);
            log.info("Note that this now gives the same results");
        }


     /*   for (int i = 1; i <= 100000; i++) {
            if (!train.hasNext()) {
                train.reset();
            }
            INDArray trueExp = train.next().getFeatures();
           *//* System.out.println(trueExp);
*//*
           *//* long[] shape = trueExp.shape();
            for(int j=0;j<shape[0];j++){
                trueExp.getRow(j).subi(0.5);
            }*//*
            System.out.println(trueExp.sumNumber().doubleValue());
            System.out.println("===============================");
        }*/
    }
}
