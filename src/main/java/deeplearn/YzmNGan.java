package deeplearn;

import org.apache.commons.lang3.ArrayUtils;
import org.datavec.api.io.labels.ParentPathLabelGenerator;
import org.datavec.api.split.FileSplit;
import org.datavec.image.loader.NativeImageLoader;
import org.datavec.image.recordreader.ImageRecordReader;
import org.deeplearning4j.datasets.datavec.RecordReaderDataSetIterator;
import org.deeplearning4j.datasets.iterator.impl.MnistDataSetIterator;
import org.deeplearning4j.nn.conf.GradientNormalization;
import org.deeplearning4j.nn.conf.MultiLayerConfiguration;
import org.deeplearning4j.nn.conf.NeuralNetConfiguration;
import org.deeplearning4j.nn.conf.layers.*;
import org.deeplearning4j.nn.conf.layers.misc.FrozenLayerWithBackprop;
import org.deeplearning4j.nn.multilayer.MultiLayerNetwork;
import org.deeplearning4j.nn.weights.WeightInit;
import org.deeplearning4j.optimize.listeners.PerformanceListener;
import org.nd4j.linalg.activations.Activation;
import org.nd4j.linalg.activations.impl.ActivationLReLU;
import org.nd4j.linalg.api.ndarray.INDArray;
import org.nd4j.linalg.dataset.DataSet;
import org.nd4j.linalg.dataset.api.iterator.DataSetIterator;
import org.nd4j.linalg.dataset.api.preprocessor.DataNormalization;
import org.nd4j.linalg.dataset.api.preprocessor.ImagePreProcessingScaler;
import org.nd4j.linalg.factory.Nd4j;
import org.nd4j.linalg.learning.config.Adam;
import org.nd4j.linalg.learning.config.IUpdater;
import org.nd4j.linalg.lossfunctions.LossFunctions;

import javax.swing.*;
import java.awt.*;
import java.awt.image.BufferedImage;
import java.io.File;
import java.util.Arrays;
/*
1、利用 yzm\NumbersUtils.java 工具生成数字
2、利用本工具进行gan网络的图片生成
 */
public class YzmNGan {
    private static final double LEARNING_RATE = 0.0002;
    private static final double GRADIENT_THRESHOLD = 100.0;
    private static final IUpdater UPDATER = Adam.builder().learningRate(LEARNING_RATE).beta1(0.5).build();

    static String genModel = "F:/face/gen2.zip";
    static String disModel = "F:/face/dis2.zip";
    static String ganModel = "F:/face/gan2.zip";
    static String inputDataDir = "F:/face/yzm";
    private static final int seed = 42;


    private static JFrame frame;
    private static JPanel panel;

    private static Layer[] genLayers() {
        return new Layer[] {
                new DenseLayer.Builder().nIn(100).nOut(256).weightInit(WeightInit.NORMAL).build(),
                new ActivationLayer.Builder(new ActivationLReLU(0.2)).build(),
                new DenseLayer.Builder().nIn(256).nOut(512).build(),
                new ActivationLayer.Builder(new ActivationLReLU(0.2)).build(),
                new DenseLayer.Builder().nIn(512).nOut(1024).build(),
                new ActivationLayer.Builder(new ActivationLReLU(0.2)).build(),
                new DenseLayer.Builder().nIn(1024).nOut(784).activation(Activation.TANH).build()
        };
    }

    /**
     * Returns a network config that takes in a 10x10 random number and produces a 28x28 grayscale image.
     *
     * @return config
     */
    private static MultiLayerConfiguration generator() {
        MultiLayerConfiguration conf = new NeuralNetConfiguration.Builder()
                .seed(seed)
                .updater(UPDATER)
                .gradientNormalization(GradientNormalization.RenormalizeL2PerLayer)
                .gradientNormalizationThreshold(GRADIENT_THRESHOLD)
                .weightInit(WeightInit.XAVIER)
                .activation(Activation.IDENTITY)
                .list(genLayers())
                .build();

        return conf;
    }

    private static Layer[] disLayers() {
        return new Layer[]{
                new DenseLayer.Builder().nIn(784).nOut(1024).build(),
                new ActivationLayer.Builder(new ActivationLReLU(0.2)).build(),
                new DropoutLayer.Builder(1 - 0.5).build(),
                new DenseLayer.Builder().nIn(1024).nOut(512).build(),
                new ActivationLayer.Builder(new ActivationLReLU(0.2)).build(),
                new DropoutLayer.Builder(1 - 0.5).build(),
                new DenseLayer.Builder().nIn(512).nOut(256).build(),
                new ActivationLayer.Builder(new ActivationLReLU(0.2)).build(),
                new DropoutLayer.Builder(1 - 0.5).build(),
                new OutputLayer.Builder(LossFunctions.LossFunction.XENT).nIn(256).nOut(1).activation(Activation.SIGMOID).build()
        };
    }

    private static MultiLayerConfiguration discriminator() {
        MultiLayerConfiguration conf = new NeuralNetConfiguration.Builder()
                .seed(seed)
                .updater(UPDATER)
                .gradientNormalization(GradientNormalization.RenormalizeL2PerLayer)
                .gradientNormalizationThreshold(GRADIENT_THRESHOLD)
                .weightInit(WeightInit.XAVIER)
                .activation(Activation.IDENTITY)
                .list(disLayers())
                .build();

        return conf;
    }

    private static MultiLayerConfiguration gan() {
        Layer[] genLayers = genLayers();
        Layer[] disLayers = Arrays.stream(disLayers())
                .map((layer) -> {
                    if (layer instanceof DenseLayer || layer instanceof OutputLayer) {
                        return new FrozenLayerWithBackprop(layer);
                    } else {
                        return layer;
                    }
                }).toArray(Layer[]::new);
        Layer[] layers = ArrayUtils.addAll(genLayers, disLayers);

        MultiLayerConfiguration conf = new NeuralNetConfiguration.Builder()
                .seed(seed)
                .updater(UPDATER)
                .gradientNormalization(GradientNormalization.RenormalizeL2PerLayer)
                .gradientNormalizationThreshold(GRADIENT_THRESHOLD)
                .weightInit(WeightInit.XAVIER)
                .activation(Activation.IDENTITY)
                .list(layers)
                .build();

        return conf;
    }

    public static void main(String... args) throws Exception {
        Nd4j.getMemoryManager().setAutoGcWindow(15 * 1000);

        // MnistDataSetIterator trainData = new MnistDataSetIterator(128, true, seed);
        File trainDataFile = new File(inputDataDir + "/train");
        FileSplit trainSplit = new FileSplit(trainDataFile, NativeImageLoader.ALLOWED_FORMATS);


        ParentPathLabelGenerator labelMaker = new ParentPathLabelGenerator(); // parent path as the image label
        ImageRecordReader trainRR = new ImageRecordReader(28, 28, 1,labelMaker);

       /* ImageTransform transform = new MultiImageTransform(new ShowImageTransform("Display - before "));

        //Initialize the record reader with the train data and the transform chain
        trainRR.initialize(trainSplit,transform);*/
        trainRR.initialize(trainSplit);

        DataSetIterator trainData = new RecordReaderDataSetIterator(trainRR, 99, 1, 1);
        DataNormalization scaler = new ImagePreProcessingScaler(0, 1);
        scaler.fit(trainData);
        trainData.setPreProcessor(scaler);

        MultiLayerNetwork gen = null;
        if (new File(genModel).exists()) {
            gen = MultiLayerNetwork.load(new File(genModel),true);

        } else{
            gen = new MultiLayerNetwork(generator());
        }
        MultiLayerNetwork dis = null;
        if (new File(disModel).exists()) {
            dis = MultiLayerNetwork.load((new File(disModel)), true);
        } else{
            dis = new MultiLayerNetwork(discriminator());
        }
        MultiLayerNetwork gan = null;
        if (new File(ganModel).exists()) {
            gan = MultiLayerNetwork.load((new File(ganModel)), true);
        } else{
            gan = new MultiLayerNetwork(gan());
        }

        gen.init();
        dis.init();
        gan.init();

        System.out.println(gen.summary());
        System.out.println(dis.summary());
        System.out.println(gan.summary());

        copyParams(gen, dis, gan);

        gen.setListeners(new PerformanceListener(10, true));
        dis.setListeners(new PerformanceListener(10, true));
        gan.setListeners(new PerformanceListener(10, true));

        trainData.reset();


        for (int i = 1; i<= 100000; i++) {
            if (!trainData.hasNext()) {
                trainData.reset();
            }
            // generate data
            INDArray real = trainData.next().getFeatures().muli(2).subi(1);
            int batchSize = (int) real.shape()[0];
            int shape = 28 * 28;
            real = real.reshape(batchSize,shape);

           /* INDArray[] samples1 = new INDArray[9];
            for (int k = 0; k < 9; k++) {
                //samples[k] = gen.output(input, false);
                samples1[k] = real.getRow(k);
            }
            visualize(samples1);
            Thread.sleep(111111);*/

            INDArray fakeIn = Nd4j.rand(batchSize, 100);
            INDArray fake = gan.activateSelectedLayers(0, gen.getLayers().length - 1, fakeIn);

            DataSet realSet = new DataSet(real, Nd4j.zeros(batchSize, 1));
            DataSet fakeSet = new DataSet(fake, Nd4j.ones(batchSize, 1));

            DataSet data = DataSet.merge(Arrays.asList(realSet, fakeSet));

            dis.fit(data);
            dis.fit(data);

            // Update the discriminator in the GAN network
            updateGan(gen, dis, gan);

            gan.fit(new DataSet(Nd4j.rand(batchSize, 100), Nd4j.zeros(batchSize, 1)));



            if (i % 10 == 1) {
                System.out.println("Iteration " + i/10+ " Visualizing...");
                INDArray[] samples = new INDArray[9];
                DataSet fakeSet2 = new DataSet(fakeIn, Nd4j.ones(batchSize, 1));

                for (int k = 0; k < 9; k++) {
                    INDArray input = fakeSet2.get(k).getFeatures();
                    //samples[k] = gen.output(input, false);
                    samples[k] = gan.activateSelectedLayers(0, gen.getLayers().length - 1, input);
                }
                visualize(samples);
            }
            // Copy the GANs generator to gen.
            updateGen(gen, gan);
            if (i % 100 == 0) {
                gen.save(new File(genModel), true);
                dis.save(new File(disModel), true);
                gan.save(new File(ganModel), true);

            }
        }
       // ModelSerializer.writeModel(gan, new File(ganModel + "/minist-model.zip"), true);//保存训练好的网络

        //gen.save(new File("mnist-mlp-generator.dlj"));
    }

    private static void copyParams(MultiLayerNetwork gen, MultiLayerNetwork dis, MultiLayerNetwork gan) {
        int genLayerCount = gen.getLayers().length;
        for (int i = 0; i < gan.getLayers().length; i++) {
            if (i < genLayerCount) {
                gen.getLayer(i).setParams(gan.getLayer(i).params());
            } else {
                dis.getLayer(i - genLayerCount).setParams(gan.getLayer(i).params());
            }
        }
    }

    private static void updateGen(MultiLayerNetwork gen, MultiLayerNetwork gan) {
        for (int i = 0; i < gen.getLayers().length; i++) {
            gen.getLayer(i).setParams(gan.getLayer(i).params());
        }
    }

    private static void updateGan(MultiLayerNetwork gen, MultiLayerNetwork dis, MultiLayerNetwork gan) {
        int genLayerCount = gen.getLayers().length;
        for (int i = genLayerCount; i < gan.getLayers().length; i++) {
            gan.getLayer(i).setParams(dis.getLayer(i - genLayerCount).params());
        }
    }

    private static void visualize(INDArray[] samples) {
        if (frame == null) {
            frame = new JFrame();
            frame.setTitle("Viz");
            frame.setDefaultCloseOperation(WindowConstants.DISPOSE_ON_CLOSE);
            frame.setLayout(new BorderLayout());

            panel = new JPanel();

            panel.setLayout(new GridLayout(samples.length / 3, 1, 8, 8));
            frame.add(panel, BorderLayout.CENTER);
            frame.setVisible(true);
        }

        panel.removeAll();

        for (INDArray sample : samples) {
            panel.add(getImage(sample));
        }

        frame.revalidate();
        frame.pack();
    }

    private static JLabel getImage(INDArray tensor) {
        BufferedImage bi = new BufferedImage(28, 28, BufferedImage.TYPE_BYTE_GRAY);
        tensor  =  tensor.reshape(28, 28).transpose();
        long[] shape = tensor.shape();
        int height = (int)shape[0];
        int width = (int)shape[1];
        for (int x = 0; x < width; x++) {
            for (int y = 0; y < height; y++) {
                int pixel = (int)(((tensor.getDouble(x,y) + 1) * 2) * 255);
                bi.getRaster().setSample(x, y, 0, pixel);
               /* System.out.println(tensor.getDouble(x,y) + " ===" + pixel);*/
            }
        }
        /*for (int i = 0; i < 784; i++) {
            int pixel = (int)(((tensor.getDouble(i) + 1) * 2) * 255);
            System.out.println(tensor.getDouble(i) + " ===" + pixel);
            bi.getRaster().setSample(i % 28, i / 28, 0, pixel);
        }*/
        //ImageIcon orig = new ImageIcon(imageFromINDArray(tensor));
        ImageIcon orig = new ImageIcon(bi);
        Image imageScaled = orig.getImage().getScaledInstance((8 * 28), (8 * 28), Image.SCALE_REPLICATE);

        ImageIcon scaled = new ImageIcon(imageScaled);

        return new JLabel(scaled);
    }
    private static BufferedImage imageFromINDArray(INDArray array) {
        array = array.reshape(28, 28);
        long[] shape = array.shape();
        int height = (int)shape[0];
        int width = (int)shape[1];
        BufferedImage image = new BufferedImage(width, height, BufferedImage.TYPE_BYTE_GRAY);
        for (int x = 0; x < width; x++) {
            for (int y = 0; y < height; y++) {
                System.out.println(array.getDouble(0, 0, y, x));
                int gray = (int) ((array.getDouble(0, 0, y, x)  + 1) * 127.5);

                // handle out of bounds pixel values
                gray = Math.min(gray, 255);
                gray = Math.max(gray, 0);

                image.getRaster().setSample(x, y, 0, gray);
            }
        }
        return image;
    }
}
