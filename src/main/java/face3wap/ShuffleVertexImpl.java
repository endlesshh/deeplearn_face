/*******************************************************************************
 * Copyright (c) 2015-2018 Skymind, Inc.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Apache License, Version 2.0 which is available at
 * https://www.apache.org/licenses/LICENSE-2.0.
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
 * License for the specific language governing permissions and limitations
 * under the License.
 *
 * SPDX-License-Identifier: Apache-2.0
 ******************************************************************************/

package face3wap;

import org.deeplearning4j.nn.api.Layer;
import org.deeplearning4j.nn.api.MaskState;
import org.deeplearning4j.nn.gradient.Gradient;
import org.deeplearning4j.nn.graph.ComputationGraph;
import org.deeplearning4j.nn.graph.vertex.BaseGraphVertex;
import org.deeplearning4j.nn.graph.vertex.VertexIndices;
import org.deeplearning4j.nn.workspace.ArrayType;
import org.deeplearning4j.nn.workspace.LayerWorkspaceMgr;
import org.nd4j.linalg.api.buffer.DataType;
import org.nd4j.linalg.api.ndarray.INDArray;
import org.nd4j.common.primitives.Pair;
import org.nd4j.linalg.factory.Nd4j;

/**
 * Adds the ability to reshape and flatten the tensor in the computation graph. This is the equivalent
 * of calling {@code .reshape(new int[]{})} on the input array to the vertex and passing the new shape
 * to the next layer. ReshapeVertex also ensures the shape is valid for the backward pass.
 *
 * @author Justin Long (crockpotveggies)
 */
public class ShuffleVertexImpl extends BaseGraphVertex {

    private char order;
    private int[] newShape;
    private int[] maskShape;


    public ShuffleVertexImpl(ComputationGraph graph, String name, int vertexIndex, char order, int[] newShape, int[] maskShape, DataType dataType) {
        this(graph, name, vertexIndex, null, null, order, newShape, maskShape, dataType);
    }

    public ShuffleVertexImpl(ComputationGraph graph, String name, int vertexIndex, VertexIndices[] inputVertices,
                             VertexIndices[] outputVertices, char order, int[] newShape, int[] maskShape, DataType dataType) {
        super(graph, name, vertexIndex, inputVertices, outputVertices, dataType);
        this.order = order;
        this.newShape = newShape;
        this.maskShape = maskShape;
    }

    @Override
    public boolean hasLayer() {
        return false;
    }

    @Override
    public Layer getLayer() {
        return null;
    }

    @Override
    public INDArray doForward(boolean training, LayerWorkspaceMgr workspaceMgr) {
        if (!canDoForward())
            throw new IllegalStateException("Cannot do forward pass: inputs not set");

        if (inputs.length > 1)
            throw new IllegalStateException("Shuffle vertex requires a single input.");
        int ratio = 2;

        long[] shape = inputs[0].shape();

        //原始大小
        // System.out.println("Shuffle前bacth=:"+shape[0]+"=:"+shape[1]+"=:"+shape[2]+"=:"+shape[3]);

       /* int batch = (int)shape[0];
        int height = (int)shape[2] * ratio;
        int width = (int)shape[3] * ratio;
        int channels = (int)shape[1] / ratio / ratio ;

        out = inputs[0].reshape( new long[]{batch_size, rh, rw, oc, h, w))
        out = K.permute_dimensions(out, (0, 3, 4, 1, 5, 2))
        out = K.reshape(out, (batch_size, oc, oh, ow))

        INDArray shuffled  = Nd4j.zeros(batch, channels,height, width);
        for(int h=0;h<batch;h++){
            for(int i=0;i<height;i++){
                for(int j=0;j<width;j++){
                    for(int k=0;k<channels;k++){
                        //每一个像素 都是三通道叠加
                        shuffled.putScalar(new int[]{h,k,i,j},inputs[0].getInt(h,k * ratio * ratio + (i % ratio) * ratio + (j % ratio),i / ratio,j / ratio));
                        //shuffled[i,j,k] = inputs[0][i / ratio,j / ratio,k * ratio * ratio + (i % ratio) * ratio + (j % ratio)];
                    }
                }
            }
        }
        inputs[0] = shuffled;*/
        int batch = (int)shape[0];
        int height = (int)shape[2] * ratio;
        int width = (int)shape[3] * ratio;
        int channels = (int)shape[1] / ratio / ratio ;

        long batch_size=shape[0], c = shape[1], h = shape[2], w = shape[3];
        long rh = ratio, rw = ratio;
        long oh =h * rh, ow =  w * rw;
        long oc = c / (rh * rw);
        INDArray out = inputs[0].reshape(new long[]{batch_size, rh, rw, oc, h, w});
        out = out.permute(new int[]{0, 3, 4, 1, 5, 2});
        out = out.reshape(new long[]{batch_size, oc, oh, ow});
        //原始大小
        //shape = out.shape();
        //System.out.println("Shuffle后bacth=:"+shape[0]+"=:"+shape[1]+"=:"+shape[2]+"=:"+shape[3]);
        return workspaceMgr.dup(ArrayType.ACTIVATIONS, out);
    }

    @Override
    public Pair<Gradient, INDArray[]> doBackward(boolean tbptt, LayerWorkspaceMgr workspaceMgr) {
        if (!canDoBackward())
            throw new IllegalStateException("Cannot do backward pass: errors not set");

        INDArray[] out = new INDArray[1];
        out[0] = workspaceMgr.dup(ArrayType.ACTIVATION_GRAD, epsilon.reshape(order, inputs[0].shape()));
        return new Pair<>(null, out);
    }

    @Override
    public void setBackpropGradientsViewArray(INDArray backpropGradientsViewArray) {
        if (backpropGradientsViewArray != null)
            throw new RuntimeException("Vertex does not have gradients; gradients view array cannot be set here");
    }

    @Override
    public Pair<INDArray, MaskState> feedForwardMaskArrays(INDArray[] maskArrays, MaskState currentMaskState,
                    int minibatchSize) {
        if (maskArrays == null || maskArrays.length < 1 || maskArrays[0] == null) {
            return new Pair<>(null, currentMaskState);
        }

        if(maskShape != null){
            return new Pair<>(maskArrays[0].reshape(order, maskShape), currentMaskState);
        }

        //Mask array is an input mask. Therefore: 2 possible cases
        //(a) column vector mask (MLP, CNN), and
        //  i. output is rank 2 or 4 (MLP, CNN) -> no change
        // ii. output is rank 3 (RNN) -> to 2d
        //(b) 2d mask (RNN), and
        //  i. output is rank 2 or 4 (MLP, CNN) -> mask to column vector
        // ii. output is rank 3 (RNN) -> no change


        if(maskArrays[0].isColumnVectorOrScalar()){
            if(newShape.length == 2 || newShape.length == 4){
                return new Pair<>(maskArrays[0], currentMaskState);
            } else if(newShape.length == 3) {
                //Column vector -> 2d (FF -> RNN etc)
                int[] newMaskShape = new int[]{newShape[0], newShape[2]};
                return new Pair<>(maskArrays[0].reshape(order, newMaskShape), currentMaskState);
            }
        } else {
            if(newShape.length == 3){
                return new Pair<>(maskArrays[0], currentMaskState);
            } else {
                //RNN -> FF/CNN
                int[] newMaskShape = new int[]{newShape[0]*newShape[2], 1};
                return new Pair<>(maskArrays[0].reshape(order, newMaskShape), currentMaskState);
            }
        }

        //Other unknown case - shouldn't happen...
        return new Pair<>(maskArrays[0], currentMaskState);
    }

    @Override
    public String toString() {
        return "ShuffleVertex(id=" + this.getVertexIndex() + ",name=\"" + this.getVertexName() + "\",shape="
                        + newShape.toString() + ")";
    }
}
