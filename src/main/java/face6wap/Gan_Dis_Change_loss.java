package face6wap;

import face5wap.LossGradientPenalty;
import org.deeplearning4j.datasets.iterator.impl.MnistDataSetIterator;
import org.deeplearning4j.nn.conf.BackpropType;
import org.deeplearning4j.nn.conf.ComputationGraphConfiguration.GraphBuilder;
import org.deeplearning4j.nn.conf.NeuralNetConfiguration;
import org.deeplearning4j.nn.conf.graph.StackVertex;
import org.deeplearning4j.nn.conf.layers.DenseLayer;
import org.deeplearning4j.nn.conf.layers.OutputLayer;
import org.deeplearning4j.nn.graph.ComputationGraph;
import org.deeplearning4j.nn.weights.WeightInit;
import org.deeplearning4j.optimize.listeners.ScoreIterationListener;
import org.nd4j.linalg.activations.Activation;
import org.nd4j.linalg.api.ndarray.INDArray;
import org.nd4j.linalg.api.rng.distribution.impl.NormalDistribution;
import org.nd4j.linalg.api.rng.distribution.impl.UniformDistribution;
import org.nd4j.linalg.dataset.MultiDataSet;
import org.nd4j.linalg.dataset.api.iterator.DataSetIterator;
import org.nd4j.linalg.factory.Nd4j;
import org.nd4j.linalg.learning.config.Adam;
import org.nd4j.linalg.learning.config.RmsProp;
import org.nd4j.linalg.learning.config.Sgd;
import org.nd4j.linalg.lossfunctions.LossFunctions;
import util.ShowUtils;
import util.ShowUtilsHanf;

import java.io.File;
import java.util.Map;

/**
 https://github.com/tjwei/GANotebooks/blob/master/wgan2-keras.ipynb

 根据此代码进修改wgan-gp
 https://github.com/eriklindernoren/Keras-GAN/blob/master/wgan_gp/wgan_gp.py
 */
public class Gan_Dis_Change_loss {

	static double lr = 0.0002;
	static String model = "F:/face/gan.zip";
	public static void main(String[] args) throws Exception {
		Nd4j.getMemoryManager().setAutoGcWindow(15 * 1000);

		final GraphBuilder dis = new NeuralNetConfiguration.Builder().updater(new RmsProp(lr))
				.weightInit(WeightInit.XAVIER).graphBuilder().backpropType(BackpropType.Standard)
				.addInputs("input1", "input2")
				.addVertex("stack", new StackVertex(), "input2", "input1")
				.addLayer("d1",
						new DenseLayer.Builder().nIn(28 * 28).nOut(256).activation(Activation.RELU)
								.weightInit(WeightInit.XAVIER).build(),
						"stack")
				.addLayer("d2",
						new DenseLayer.Builder().nIn(256).nOut(128).activation(Activation.RELU)
								.weightInit(WeightInit.XAVIER).build(),
						"d1")
				.addLayer("d3",
						new DenseLayer.Builder().nIn(128).nOut(128).activation(Activation.RELU)
								.weightInit(WeightInit.XAVIER).build(),
						"d2")
				.addLayer("out", new OutputLayer.Builder(new LossGradientPenalty()).nIn(128).nOut(1)
						.activation(Activation.TANH).build(), "d3")
				.setOutputs("out");

		final GraphBuilder gen = new NeuralNetConfiguration.Builder().updater(new RmsProp(lr))
				.weightInit(WeightInit.XAVIER).graphBuilder().backpropType(BackpropType.Standard)
				.addInputs("input1", "input2")
				.addLayer("g1",
						new DenseLayer.Builder().nIn(28 * 28).nOut(128).activation(Activation.RELU)
								.weightInit(WeightInit.XAVIER).build(),
						"input1")
				.addLayer("g2",
						new DenseLayer.Builder().nIn(128).nOut(512).activation(Activation.RELU)
								.weightInit(WeightInit.XAVIER).build(),
						"g1")
				.addLayer("g3",
						new DenseLayer.Builder().nIn(512).nOut(28 * 28).activation(Activation.RELU)
								.weightInit(WeightInit.XAVIER).build(),
						"g2")
				.addVertex("stack", new StackVertex(), "input2", "g3")
				.addLayer("d1",
						new DenseLayer.Builder().nIn(28 * 28).nOut(256).activation(Activation.RELU)
								.weightInit(WeightInit.XAVIER).build(),
						"stack")
				.addLayer("d2",
						new DenseLayer.Builder().nIn(256).nOut(128).activation(Activation.RELU)
								.weightInit(WeightInit.XAVIER).build(),
						"d1")
				.addLayer("d3",
						new DenseLayer.Builder().nIn(128).nOut(128).activation(Activation.RELU)
								.weightInit(WeightInit.XAVIER).build(),
						"d2")
				.addLayer("out", new OutputLayer.Builder(LossFunctions.LossFunction.WASSERSTEIN).nIn(128).nOut(1)
						.activation(Activation.TANH).build(), "d3")
				.setOutputs("out");

		ComputationGraph net = new ComputationGraph(dis.build());
		ComputationGraph net1 = new ComputationGraph(gen.build());

		net.init();
		net1.init();

		System.out.println(net.summary());
		System.out.println(net1.summary());

		net.setListeners(new ScoreIterationListener(100));
		DataSetIterator train = new MnistOneDataSetIterator(30, true, 12345);
		//按垂直方向（行顺序）堆叠数组构成一个新的数组

		INDArray valid_fake = Nd4j.vstack(Nd4j.ones(30, 1).muli(-1), Nd4j.ones(30, 1));
		INDArray valid = Nd4j.vstack(Nd4j.ones(30, 1).muli(-1), Nd4j.ones(30, 1).muli(-1));
		INDArray dummy = Nd4j.zeros(30 * 2, 1);//  np.zeros((batch_size, 1)) # Dummy gt for gradient penalty


		for (int i = 1; i <= 100000; i++) {
			if (!train.hasNext()) {
				train.reset();
			}

			for(int m=0;m<5;m++){
				INDArray real_img  = train.next().getFeatures();
				INDArray z_disc  = Nd4j.rand(new NormalDistribution(),new long[] { 30, 28 * 28 });
				INDArray fake_img  = net1.feedForward(new INDArray[] {z_disc,z_disc}, false).get("g3");// .reshape(20,28,28);

				MultiDataSet realFakeData = new MultiDataSet(new INDArray[] {real_img, fake_img},new INDArray[] { valid_fake });
				trainD(net, realFakeData);

				INDArray interpolated_img = randomWeightedAverage(30,real_img,fake_img);
				MultiDataSet interpolatedD = new MultiDataSet(new INDArray[]{interpolated_img, interpolated_img},new INDArray[]{dummy});

				trainD(net, interpolatedD);
			}
			net1.getLayer("d1").setParam("W", net.getLayer("d1").getParam("W"));
			net1.getLayer("d1").setParam("b", net.getLayer("d1").getParam("b"));
			net1.getLayer("d2").setParam("W", net.getLayer("d2").getParam("W"));
			net1.getLayer("d2").setParam("b", net.getLayer("d2").getParam("b"));
			net1.getLayer("d3").setParam("W", net.getLayer("d3").getParam("W"));
			net1.getLayer("d3").setParam("b", net.getLayer("d3").getParam("b"));
			net1.getLayer("out").setParam("W", net.getLayer("out").getParam("W"));
			net1.getLayer("out").setParam("b", net.getLayer("out").getParam("b"));

			INDArray noize = Nd4j.rand(new NormalDistribution(),new long[] { 30, 28 * 28 });
			//INDArray valid =  net.feedForward(new INDArray[] {noize,noize}, false).get("out");// .reshape(20,28,28);
			MultiDataSet dataSetG = new MultiDataSet(new INDArray[]{noize, noize},
					new INDArray[]{valid});
	//Nd4j.hstack(
			trainG(net1, dataSetG);


		/*	net.getLayer("g1").setParam("W", net1.getLayer("g1").getParam("W"));
			net.getLayer("g1").setParam("b", net1.getLayer("g1").getParam("b"));
			net.getLayer("g2").setParam("W", net1.getLayer("g2").getParam("W"));
			net.getLayer("g2").setParam("b", net1.getLayer("g2").getParam("b"));
			net.getLayer("g3").setParam("W", net1.getLayer("g3").getParam("W"));
			net.getLayer("g3").setParam("b", net1.getLayer("g3").getParam("b"));*/

			if (i % 10 == 0) {

				INDArray noise =  Nd4j.rand(new NormalDistribution(),new long[] { 50, 28 * 28 });
				INDArray noise1 =  Nd4j.rand(new NormalDistribution(),new long[] { 50, 28 * 28 });
				/*INDArray[] samps =  gen.output(noise);*/
				/*long[] shpaes = samps[0].shape();
				INDArray[] samples = new INDArray[(int)samps.length];
				for (int j = 0; j < samps.length; j++) {
					samples[j] = samps[j];
				}*/
				INDArray indArray2 = net1.feedForward(new INDArray[] {noise,noise1}, false).get("g3");// .reshape(20,28,28);
				INDArray[] samples = new INDArray[(int)indArray2.size(0)];

				samples[0] = indArray2;

				ShowUtilsHanf.visualize(samples,"拆分");
			}
			if (i % 10000 == 0) {
			   net.save(new File(model), true);
			}
 
		}
 
	}

	public static INDArray randomWeightedAverage(int batch,INDArray real,INDArray fake){
		// INDArray alpha = Nd4j.rand(new UniformDistribution(0,1),new long[]{batch, 1, 1, 1});// new NDRandom().uniform(32, 1, DataType.FLOAT, new long[]{32, 1, 1, 1});
		INDArray alpha = Nd4j.rand(new UniformDistribution(0,1),new long[]{batch, 784});// new NDRandom().uniform(32, 1, DataType.FLOAT, new long[]{32, 1, 1, 1});
		return (alpha.muli(real)).addi((Nd4j.ones(alpha.shape()).subi(alpha)).muli(fake));
	}
 	// 判别模型  D(x)
	public static void trainD(ComputationGraph net, MultiDataSet dataSet) {
		net.setLearningRate("d1", lr);
		net.setLearningRate("d2", lr);
		net.setLearningRate("d3", lr);
		net.setLearningRate("out", lr);
		net.fit(dataSet);
	}
	//生成模型 g(z)
	public static void trainG(ComputationGraph net, MultiDataSet dataSet) {
		net.setLearningRate("g1", lr);
		net.setLearningRate("g2", lr);
		net.setLearningRate("g3", lr);
		net.setLearningRate("d1", 0);
		net.setLearningRate("d2", 0);
		net.setLearningRate("d3", 0);
		net.setLearningRate("out", 0);
		net.fit(dataSet);
	}
}