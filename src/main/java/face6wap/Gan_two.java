package face6wap;

import org.deeplearning4j.datasets.iterator.impl.MnistDataSetIterator;
import org.deeplearning4j.nn.conf.BackpropType;
import org.deeplearning4j.nn.conf.ComputationGraphConfiguration.GraphBuilder;
import org.deeplearning4j.nn.conf.NeuralNetConfiguration;
import org.deeplearning4j.nn.conf.graph.StackVertex;
import org.deeplearning4j.nn.conf.layers.DenseLayer;
import org.deeplearning4j.nn.conf.layers.OutputLayer;
import org.deeplearning4j.nn.graph.ComputationGraph;
import org.deeplearning4j.nn.weights.WeightInit;
import org.deeplearning4j.optimize.listeners.ScoreIterationListener;
import org.nd4j.linalg.activations.Activation;
import org.nd4j.linalg.api.ndarray.INDArray;
import org.nd4j.linalg.api.rng.distribution.impl.NormalDistribution;
import org.nd4j.linalg.dataset.DataSet;
import org.nd4j.linalg.dataset.MultiDataSet;
import org.nd4j.linalg.dataset.api.iterator.DataSetIterator;
import org.nd4j.linalg.factory.Nd4j;
import org.nd4j.linalg.learning.config.Sgd;
import org.nd4j.linalg.lossfunctions.LossFunctions;
import util.ShowUtils;
import util.ShowUtilsT;

import java.io.File;
import java.util.Arrays;
import java.util.Map;

/**
 * 说明：
	该方法是由 Gan_one变化而来，说明了ComputationGraph 中dis的拆分和参数的更新
 */
public class Gan_two {

	static double lr = 0.01;
	static String model = "F:/face/gan.zip";
	public static void main(String[] args) throws Exception {
		Nd4j.getMemoryManager().setAutoGcWindow(15 * 1000);
		final GraphBuilder ganModel = new NeuralNetConfiguration.Builder()
				.weightInit(WeightInit.XAVIER).graphBuilder().backpropType(BackpropType.Standard)
				.addInputs("input1")
				.addLayer("g1",
						new DenseLayer.Builder().updater(new Sgd(lr)).nIn(10).nOut(128).activation(Activation.RELU)
								.weightInit(WeightInit.XAVIER).build(),"input1")
				.addLayer("g2",
						new DenseLayer.Builder().updater(new Sgd(lr)).nIn(128).nOut(512).activation(Activation.RELU)
								.weightInit(WeightInit.XAVIER).build(),"g1")
				.addLayer("g3",
						new DenseLayer.Builder().updater(new Sgd(lr)).nIn(512).nOut(28 * 28).activation(Activation.RELU)
								.weightInit(WeightInit.XAVIER).build(),"g2")
				//.addVertex("stack", new StackVertex(), "input2", "g3")
				.addLayer("d1",
						new DenseLayer.Builder().updater(new Sgd(0)).nIn(28 * 28).nOut(256).activation(Activation.RELU)
								.weightInit(WeightInit.XAVIER).build(),"g3")
				.addLayer("d2",
						new DenseLayer.Builder().updater(new Sgd(0)).nIn(256).nOut(128).activation(Activation.RELU)
								.weightInit(WeightInit.XAVIER).build(),"d1")
				.addLayer("d3",
						new DenseLayer.Builder().updater(new Sgd(0)).nIn(128).nOut(128).activation(Activation.RELU)
								.weightInit(WeightInit.XAVIER).build(),"d2")
				.addLayer("out", new OutputLayer.Builder(LossFunctions.LossFunction.XENT).updater(new Sgd(0)).nIn(128).nOut(1)
						.activation(Activation.SIGMOID).build(), "d3")
				.setOutputs("out");
		//拆分出的dis
		final GraphBuilder disModel = new NeuralNetConfiguration.Builder().updater(new Sgd(lr))
				.weightInit(WeightInit.XAVIER).graphBuilder().backpropType(BackpropType.Standard)
				.addInputs("input1")
				.addLayer("d1",new DenseLayer.Builder().nIn(28 * 28).nOut(256).activation(Activation.RELU)
								.weightInit(WeightInit.XAVIER).build(),"input1")
				.addLayer("d2",new DenseLayer.Builder().nIn(256).nOut(128).activation(Activation.RELU)
								.weightInit(WeightInit.XAVIER).build(),"d1")
				.addLayer("d3",new DenseLayer.Builder().nIn(128).nOut(128).activation(Activation.RELU)
								.weightInit(WeightInit.XAVIER).build(),"d2")
				.addLayer("out", new OutputLayer.Builder(LossFunctions.LossFunction.XENT).nIn(128).nOut(1)
						.activation(Activation.SIGMOID).build(), "d3")
				.setOutputs("out");

		final GraphBuilder genModel = new NeuralNetConfiguration.Builder().updater(new Sgd(0))
				.weightInit(WeightInit.XAVIER).graphBuilder().backpropType(BackpropType.Standard)
				.addInputs("input1")
				.addLayer("g1",
						new DenseLayer.Builder().nIn(10).nOut(128).activation(Activation.RELU).weightInit(WeightInit.XAVIER).build(),"input1")
				.addLayer("g2",
						new DenseLayer.Builder().nIn(128).nOut(512).activation(Activation.RELU).weightInit(WeightInit.XAVIER).build(),"g1")
				.addLayer("g3",
						new DenseLayer.Builder().nIn(512).nOut(28 * 28).activation(Activation.RELU).weightInit(WeightInit.XAVIER).build(),"g2")
				.setOutputs("g3");
		ComputationGraph net = new ComputationGraph(ganModel.build());
		ComputationGraph gen = new ComputationGraph(genModel.build());
		ComputationGraph dis = new ComputationGraph(disModel.build());
		/*if (new File(model).exists()) {
			net = ComputationGraph.load(new File(model), true);
		}else{*/

		/*}*/
		net.init();
		gen.init();
		dis.init();
		System.out.println(net.summary());
		System.out.println(gen.summary());
		System.out.println(dis.summary());
		/*UIServer uiServer = UIServer.getInstance();
		StatsStorage statsStorage = new InMemoryStatsStorage();
		uiServer.attach(statsStorage);*/
		net.setListeners(new ScoreIterationListener(100));




		DataSetIterator train = new MnistDataSetIterator(30, true, 12345);
		//按垂直方向（行顺序）堆叠数组构成一个新的数组
		INDArray labelD = Nd4j.vstack(Nd4j.ones(30, 1), Nd4j.zeros(30, 1));
 
		INDArray labelG = Nd4j.ones(60, 1);
		INDArray realLabel =  Nd4j.zeros(30, 1);
		INDArray fakeLabel = Nd4j.ones(30, 1);
		for (int i = 1; i <= 100000; i++) {
			if (!train.hasNext()) {
				train.reset();
			}
			INDArray trueExp = train.next().getFeatures();

			INDArray z = Nd4j.rand(new NormalDistribution(),new long[] { 30, 10 });
			Map<String, INDArray> mapfake = gen.feedForward(
					new INDArray[] {z}, false);
			INDArray fake = mapfake.get("g3");// .reshape(20,28,28);

			DataSet realSet = new DataSet(trueExp, realLabel);
			DataSet fakeSet = new DataSet(fake,fakeLabel);

			DataSet data = DataSet.merge(Arrays.asList(realSet, fakeSet));

			for(int m=0;m<10;m++){
				dis.fit(data);
				//trainD(net, dataSetD);
			}
			
			net.getLayer("d1").setParam("W", dis.getLayer("d1").getParam("W"));
			net.getLayer("d1").setParam("b", dis.getLayer("d1").getParam("b"));
			net.getLayer("d2").setParam("W", dis.getLayer("d2").getParam("W"));
			net.getLayer("d2").setParam("b", dis.getLayer("d2").getParam("b"));
			net.getLayer("d3").setParam("W", dis.getLayer("d3").getParam("W"));
			net.getLayer("d3").setParam("b", dis.getLayer("d3").getParam("b"));
			net.getLayer("out").setParam("W", dis.getLayer("out").getParam("W"));
			net.getLayer("out").setParam("b", dis.getLayer("out").getParam("b"));


			z = Nd4j.rand(new NormalDistribution(),new long[] { 30, 10 });
		/*	MultiDataSet dataSetG = new MultiDataSet(new INDArray[] { z, trueExp },
					new INDArray[] { labelG });*/

			DataSet dataSetG = new DataSet(z, fakeLabel);
			net.fit(dataSetG);

			//trainG(net, dataSetG);

			gen.getLayer("g1").setParam("W", net.getLayer("g1").getParam("W"));
			gen.getLayer("g1").setParam("b", net.getLayer("g1").getParam("b"));
			gen.getLayer("g2").setParam("W", net.getLayer("g2").getParam("W"));
			gen.getLayer("g2").setParam("b", net.getLayer("g2").getParam("b"));
			gen.getLayer("g3").setParam("W", net.getLayer("g3").getParam("W"));
			gen.getLayer("g3").setParam("b", net.getLayer("g3").getParam("b"));

			if (i % 50 == 0) {

				INDArray noise =  Nd4j.rand(new NormalDistribution(),new long[] { 50, 10 });
				/*INDArray[] samps =  gen.output(noise);*/
				/*long[] shpaes = samps[0].shape();
				INDArray[] samples = new INDArray[(int)samps.length];
				for (int j = 0; j < samps.length; j++) {
					samples[j] = samps[j];
				}*/

				Map<String, INDArray> map1 = gen.feedForward(
						new INDArray[] {noise}, false);
				INDArray indArray2 = map1.get("g3");// .reshape(20,28,28);
				INDArray[] samples = new INDArray[(int)indArray2.size(0)];

				samples[0] = indArray2;

				ShowUtils.visualize(samples,"拆分");

				Map<String, INDArray> map = net.feedForward(
						new INDArray[] {noise}, false);
				INDArray indArray = map.get("g3");// .reshape(20,28,28);
				INDArray[] sampless = new INDArray[(int)indArray.size(0)];

				sampless[0] = indArray;

				ShowUtilsT.visualize(sampless,"获取");
			}


			if (i % 10000 == 0) {
			   net.save(new File(model), true);
			}
 
		}
 
	}
 	// 判别模型  D(x)
	public static void trainD(ComputationGraph net, MultiDataSet dataSet) {
		net.setLearningRate("g1", 0);
		net.setLearningRate("g2", 0);
		net.setLearningRate("g3", 0);
		net.setLearningRate("d1", lr);
		net.setLearningRate("d2", lr);
		net.setLearningRate("d3", lr);
		net.setLearningRate("out", lr);
		net.fit(dataSet);
	}
	//生成模型 g(z)
	public static void trainG(ComputationGraph net, MultiDataSet dataSet) {
		net.setLearningRate("g1", lr);
		net.setLearningRate("g2", lr);
		net.setLearningRate("g3", lr);
		net.setLearningRate("d1", 0);
		net.setLearningRate("d2", 0);
		net.setLearningRate("d3", 0);
		net.setLearningRate("out", 0);
		net.fit(dataSet);
	}
}