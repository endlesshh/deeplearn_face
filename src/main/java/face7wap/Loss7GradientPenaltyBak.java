/*******************************************************************************
 * Copyright (c) 2015-2018 Skymind, Inc.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Apache License, Version 2.0 which is available at
 * https://www.apache.org/licenses/LICENSE-2.0.
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
 * License for the specific language governing permissions and limitations
 * under the License.
 *
 * SPDX-License-Identifier: Apache-2.0
 ******************************************************************************/

package face7wap;

import lombok.EqualsAndHashCode;
import org.nd4j.autodiff.samediff.SDVariable;
import org.nd4j.autodiff.samediff.SameDiff;
import org.nd4j.common.base.Preconditions;
import org.nd4j.common.primitives.Pair;
import org.nd4j.linalg.activations.IActivation;
import org.nd4j.linalg.api.buffer.DataType;
import org.nd4j.linalg.api.ndarray.INDArray;
import org.nd4j.linalg.factory.Nd4j;
import org.nd4j.linalg.indexing.NDArrayIndex;
import org.nd4j.linalg.lossfunctions.ILossFunction;
import org.nd4j.linalg.ops.transforms.Transforms;
import org.nd4j.weightinit.impl.XavierInitScheme;

import java.util.HashMap;
import java.util.Map;

/**
 * Wasserstein loss function, which calculates the Wasserstein distance, also known as earthmover's distance.
 *
 * This is not necessarily a general purpose loss function, and is intended for use as a discriminator loss.
 *
 * When using in a discriminator, use a label of 1 for real and -1 for generated
 * instead of the 1 and 0 used in normal GANs.
 *
 * As described in <a href="https://papers.nips.cc/paper/5679-learning-with-a-wasserstein-loss.pdf">Learning with a Wasserstein Loss</a>
 *
 * @author Ryan Nett
 */
@EqualsAndHashCode(callSuper = false)
public class Loss7GradientPenaltyBak implements ILossFunction {

    public Loss7GradientPenaltyBak(INDArray interImg){
        this.img = interImg;
    }
    private INDArray img;

    private INDArray scoreArray(INDArray labels, INDArray preOutput, IActivation activationFn, INDArray mask){
        if(!labels.equalShapes(preOutput)){
            Preconditions.throwEx("Labels and preOutput must have equal shapes: got shapes %s vs %s", labels.shape(), preOutput.shape());
        }
        labels = labels.castTo(preOutput.dataType());   //No-op if already correct dtype

        long rows = labels.shape()[0];// [60,1]
        INDArray labelsF = labels.get(NDArrayIndex.interval(0,rows/3));
        INDArray labelsR = labels.get(NDArrayIndex.interval(rows/3,rows/3 * 2));
        INDArray labelsI = labels.get(NDArrayIndex.interval(rows/3 *2,rows));

        INDArray imgF = preOutput.get(NDArrayIndex.interval(0,rows/3));
        INDArray imgR = preOutput.get(NDArrayIndex.interval(rows/3,rows/3 * 2));
        INDArray imgI = preOutput.get(NDArrayIndex.interval(rows/3 *2,rows));

        INDArray outputF = imgF.dup();
        INDArray outputR = imgR.dup();

      /*  INDArray scoreArr = labelsR.mul(outputF);
        INDArray scoreBrr = labelsR.mul(outputR);*/

      /*double scoreA = scoreArr.mean().sumNumber().doubleValue();
        double scoreB = scoreBrr.mean().sumNumber().doubleValue();*/

        return outputF.sub(outputR);

        /*//Computes gradient penalty based on prediction and weighted real / fake samples
        //将一起放入的数据分开
        SameDiff sd = SameDiff.create();
        long[] shape =  labels.shape();
        SDVariable input = sd.placeHolder("input", DataType.FLOAT, labels.shape());
        SDVariable labelss = sd.placeHolder("labels", DataType.FLOAT,  labels.shape());

        //Second: let's create our variables
        SDVariable weights = sd.var("weights", new XavierInitScheme('c', shape[1], shape[1]), DataType.FLOAT, shape[1],shape[1]);

        Map<String,INDArray> placeholderData = new HashMap<>();
        placeholderData.put("input", preOutput.dup());
        placeholderData.put("labels", labels);
        //Calculate gradients:
        Map<String,INDArray> gradMap = sd.calculateGradients(placeholderData, "weights");
        *//*System.out.println("Weights gradient:");
        System.out.println(gradMap.get("weights"));
        System.out.println("Bias gradient:");
        System.out.println(gradMap.get("bias"));*//*

        INDArray gradients = gradMap.get("weights");

        // compute the euclidean norm by squaring ...
        INDArray gradients_sqr = gradients.mul(gradients);
        //  ... summing over the rows ...
        INDArray axis = Nd4j.arange(1, gradients_sqr.shape().length);
        for(int i=0;i<axis.rows();i++){
            gradients_sqr = Nd4j.sum(gradients_sqr,axis.getInt(i));
        }
        INDArray gradients_sqr_sum = gradients_sqr;
        //  ... and sqrt
        INDArray gradient_l2_norm = Transforms.sqrt(gradients_sqr_sum);
        //compute lambda * (1 - ||grad||)^2 still for each single sample
        INDArray gradient_l2_norm_sqr = Nd4j.ones(gradient_l2_norm.shape()).sub(gradient_l2_norm);
        INDArray gradient_penalty = gradient_l2_norm_sqr.mul(gradient_l2_norm_sqr);
        //return the mean as loss over all the batch samples
        if (mask != null) {
            LossUtil.applyMask(gradient_penalty, mask);
        }
        return gradient_penalty;*/
       /* return Nd4j.mean(gradient_penalty);*/

       /* labels = labels.castTo(preOutput.dataType());   //No-op if already correct dtype

        INDArray output = activationFn.getActivation(preOutput.dup(), true);

        INDArray scoreArr = labels.mul(output);
        if (mask != null) {
            LossUtil.applyMask(scoreArr, mask);
        }
        return scoreArr;*/
    }

    @Override
    public double computeScore(INDArray labels, INDArray preOutput, IActivation activationFn, INDArray mask,
            boolean average) {
        INDArray scoreArr = scoreArray(labels, preOutput, activationFn, mask);

        double score = scoreArr.mean().sumNumber().doubleValue();

        return score;
    }

    @Override
    public INDArray computeScoreArray(INDArray labels, INDArray preOutput, IActivation activationFn, INDArray mask) {
        INDArray scoreArr = scoreArray(labels, preOutput, activationFn, mask);
        return Nd4j.expandDims(scoreArr.mean(), 1);
    }

    @Override
    public INDArray computeGradient(INDArray labels, INDArray preOutput, IActivation activationFn, INDArray mask) {
        if(!labels.equalShapes(preOutput)){
            Preconditions.throwEx("Labels and preOutput must have equal shapes: got shapes %s vs %s", labels.shape(), preOutput.shape());
        }
        labels = labels.castTo(preOutput.dataType());   //No-op if already correct dtype
        long[] shape =  labels.shape();
        long rows = shape[0];// [60,1]

        INDArray labelsF = labels.get(NDArrayIndex.interval(0,rows/3));
        INDArray labelsR = labels.get(NDArrayIndex.interval(rows/3,rows/3 * 2));
        INDArray labelsI = labels.get(NDArrayIndex.interval(rows/3 *2,rows));
        INDArray imgF = preOutput.get(NDArrayIndex.interval(0,rows/3));
        INDArray imgR = preOutput.get(NDArrayIndex.interval(rows/3,rows/3 * 2));
        INDArray imgI = preOutput.get(NDArrayIndex.interval(rows/3 *2,rows));

        SameDiff sd = SameDiff.create();

        SDVariable input = sd.placeHolder("input", DataType.FLOAT,shape);
        SDVariable labelss = sd.placeHolder("labels", DataType.FLOAT,shape);
        SDVariable weights = sd.var("weights", new XavierInitScheme('c', shape[1], shape[1]), DataType.FLOAT, shape[1],shape[1]);
        SDVariable out = input.mmul(weights);
        Map<String,INDArray> placeholderData = new HashMap<>();
        placeholderData.put("input", imgI.dup());
        placeholderData.put("labels", labelsI);
        //Calculate gradients:
        Map<String,INDArray> gradMap = sd.calculateGradients(placeholderData, "weights");
        INDArray gradients = gradMap.get("weights");
        // compute the euclidean norm by squaring ...
        INDArray gradients_sqr = gradients.mul(gradients);
        //  ... summing over the rows ...
        INDArray axis = Nd4j.arange(1, gradients_sqr.shape().length);
        for(int i=0;i<axis.rows();i++){
            gradients_sqr = Nd4j.sum(gradients_sqr,axis.getInt(i));
        }
        INDArray gradients_sqr_sum = gradients_sqr;
        //  ... and sqrt
        INDArray gradient_l2_norm = Transforms.sqrt(gradients_sqr_sum);
        //compute lambda * (1 - ||grad||)^2 still for each single sample
        INDArray gradient_l2_norm_sqr = Nd4j.ones(gradient_l2_norm.shape()).sub(gradient_l2_norm);
        INDArray gradient_penalty = gradient_l2_norm_sqr.mul(gradient_l2_norm_sqr);

        return gradient_penalty;

    }

    @Override
    public Pair<Double, INDArray> computeGradientAndScore(INDArray labels, INDArray preOutput, IActivation activationFn,
            INDArray mask, boolean average) {
        return new Pair<>(computeScore(labels, preOutput, activationFn, mask, average),
                computeGradient(labels, preOutput, activationFn, mask));
    }
    @Override
    public String name() {
        return toString();
    }

    @Override
    public String toString() {
        return "Loss7GWasserstein()";
    }
}
