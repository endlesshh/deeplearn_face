/*******************************************************************************
 * Copyright (c) 2015-2018 Skymind, Inc.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Apache License, Version 2.0 which is available at
 * https://www.apache.org/licenses/LICENSE-2.0.
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
 * License for the specific language governing permissions and limitations
 * under the License.
 *
 * SPDX-License-Identifier: Apache-2.0
 ******************************************************************************/

package face7wap;

import lombok.EqualsAndHashCode;
import org.nd4j.common.primitives.Pair;
import org.nd4j.linalg.activations.IActivation;
import org.nd4j.linalg.api.ndarray.INDArray;
import org.nd4j.linalg.factory.Nd4j;
import org.nd4j.linalg.lossfunctions.ILossFunction;
import org.nd4j.linalg.ops.transforms.Transforms;

/**
 * Wasserstein loss function, which calculates the Wasserstein distance, also known as earthmover's distance.
 *
 * This is not necessarily a general purpose loss function, and is intended for use as a discriminator loss.
 *
 * When using in a discriminator, use a label of 1 for real and -1 for generated
 * instead of the 1 and 0 used in normal GANs.
 *
 * As described in <a href="https://papers.nips.cc/paper/5679-learning-with-a-wasserstein-loss.pdf">Learning with a Wasserstein Loss</a>
 *
 * @author Ryan Nett
 */
@EqualsAndHashCode(callSuper = false)
public class Loss7GradientPenaltyBak2 implements ILossFunction {

    public Loss7GradientPenaltyBak2(INDArray interImg){
        this.averaged_samples = interImg;
    }
    private INDArray averaged_samples;

    private INDArray scoreArray(INDArray labels, INDArray preOutput, IActivation activationFn, INDArray mask){


        labels = labels.castTo(preOutput.dataType());   //No-op if already correct dtype
        INDArray dLda = labels.div(labels.size(1));
        // INDArray dLda = averaged_samples.div(averaged_samples.size(1));
        INDArray gradients = activationFn.backprop(averaged_samples,dLda).getFirst();

        // compute the euclidean norm by squaring ...
        INDArray gradients_sqr = gradients.muli(gradients);
        //  ... summing over the rows ...
        //INDArray axis = Nd4j.arange(1, gradients_sqr.shape().length);
        long[] axis = gradients_sqr.shape();

        for(int i=1;i<axis.length;i++){
            gradients_sqr = gradients_sqr.cumsum(i);
        }


        INDArray gradients_sqr_sum = gradients_sqr;
        //  ... and sqrt
        INDArray gradient_l2_norm = Transforms.sqrt(gradients_sqr_sum);
        //compute lambda * (1 - ||grad||)^2 still for each single sample
        INDArray gradient_l2_norm_sqr = Nd4j.ones(gradient_l2_norm.shape()).subi(gradient_l2_norm);
        INDArray gradient_penalty = gradient_l2_norm_sqr.muli(gradient_l2_norm_sqr);
        return gradient_penalty;
    }

    @Override
    public double computeScore(INDArray labels, INDArray preOutput, IActivation activationFn, INDArray mask,
                               boolean average) {
        INDArray scoreArr = scoreArray(labels, preOutput, activationFn, mask);

        double score = scoreArr.mean(1).sumNumber().doubleValue();

        if (average) {
            score /= scoreArr.size(0);
        }
        System.out.println("gradient:"+score);
        return score;
    }

    @Override
    public INDArray computeScoreArray(INDArray labels, INDArray preOutput, IActivation activationFn, INDArray mask) {
        INDArray scoreArr = scoreArray(labels, preOutput, activationFn, mask);
        return Nd4j.expandDims(scoreArr.mean(), 1);
    }

    @Override
    public INDArray computeGradient(INDArray labels, INDArray preOutput, IActivation activationFn, INDArray mask) {

        labels = labels.castTo(preOutput.dataType());   //No-op if already correct dtype
        INDArray dLda = labels.div(labels.size(1));

        /*if (mask != null && LossUtil.isPerOutputMasking(dLda, mask)) {
            LossUtil.applyMask(labels, mask);
        }*/
       // INDArray dLda = averaged_samples.div(averaged_samples.size(1));
        INDArray gradients = activationFn.backprop(averaged_samples,dLda).getFirst();
        // compute the euclidean norm by squaring ...
        INDArray gradients_sqr = gradients.mul(gradients);
        //  ... summing over the rows ...
        //INDArray axis = Nd4j.arange(1, gradients_sqr.shape().length);
        long[] axis = gradients_sqr.shape();
        for(int i=1;i<axis.length;i++){
            gradients_sqr = gradients_sqr.cumsum(i);
        }

        INDArray gradients_sqr_sum = gradients_sqr;
        //  ... and sqrt
        INDArray gradient_l2_norm = Transforms.sqrt(gradients_sqr_sum);
        //compute lambda * (1 - ||grad||)^2 still for each single sample
        INDArray gradient_l2_norm_sqr = Nd4j.ones(gradient_l2_norm.shape()).subi(gradient_l2_norm);
        INDArray gradient_penalty = gradient_l2_norm_sqr.mul(gradient_l2_norm_sqr);
        /*INDArray gradMean = Nd4j.mean(gradient_penalty,1);
        long[] gradShape = gradMean.shape();*/
        //INDArray grad =  Nd4j.valueArrayOf(axis, gradMean.sumNumber().doubleValue());

        return gradient_penalty;

    }

    @Override
    public Pair<Double, INDArray> computeGradientAndScore(INDArray labels, INDArray preOutput, IActivation activationFn,
            INDArray mask, boolean average) {
        return new Pair<>(computeScore(labels, preOutput, activationFn, mask, average),
                computeGradient(labels, preOutput, activationFn, mask));
    }
    @Override
    public String name() {
        return toString();
    }

    @Override
    public String toString() {
        return "Loss7Gradient()";
    }
}
