package facewap;

import deeplearn.Gan;
import org.apache.commons.math3.distribution.NormalDistribution;
import org.deeplearning4j.nn.api.OptimizationAlgorithm;
import org.deeplearning4j.nn.conf.Updater;
import org.deeplearning4j.nn.conf.layers.OutputLayer;
import org.deeplearning4j.nn.graph.ComputationGraph;
import org.deeplearning4j.nn.transferlearning.FineTuneConfiguration;
import org.deeplearning4j.nn.transferlearning.TransferLearning;
import org.deeplearning4j.nn.weights.WeightInit;
import org.deeplearning4j.zoo.PretrainedType;
import org.deeplearning4j.zoo.ZooModel;
import org.deeplearning4j.zoo.model.VGG16;
import org.nd4j.linalg.activations.Activation;
import org.nd4j.linalg.lossfunctions.LossFunctions;

import java.io.File;
import java.io.IOException;

public class GetVgg16 {
    public static void main(String[] args) throws IOException {
      /*  VGG16 vg = VGG16.builder().build();
        //System.out.println(vg.pretrainedUrl(PretrainedType.VGGFACE));

        System.out.println(vg.init().summary());*/
        ComputationGraph restored = ComputationGraph.load(new File("F:/face/vgg16_dl4j_vggface_inference.v1.zip"), true);
        System.out.println(restored.summary());
    }
}
